import { CarsService } from "./CarsService.service"
import { Car } from "./Car"
export class Salon {
    constructor() {
        this.niz = [];
        this.id=0;
    }

    Dodaj(auto) {
        this.niz.push(auto);
        this.id++;
    }

    NapuniNiz() {
        let x = CarsService.get().then(car => {
            car.forEach(auto => {
                let a = new Car(auto.id, auto.name, auto.model, auto.year, auto.owner, auto.regN, auto.cena);
                this.Dodaj(a);
            });
         this.id=this.niz[this.niz.length-1].id+1;
        });
    }

    IzbrisiElement(reg) {

        let indeks = 0;
        for (let i = 0; i < this.niz.length; i++) {
            if (this.niz[i].regN === reg)
                indeks = i;
        }
        this.niz.splice(indeks, 1);
    }

    AzurirajElement(id,ime,model,year,owner,reg,cena){
        let identifikator=this.niz.findIndex(el=>el.id===id);
        this.niz[identifikator].ime= ime;
        this.niz[identifikator].model=model;
        this.niz[identifikator].year= year;
        this.niz[identifikator].owner=owner;
        this.niz[identifikator].regN=reg;
        this.niz[identifikator].cena=cena;
    }

}