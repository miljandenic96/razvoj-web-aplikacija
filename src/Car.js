import * as Rxjs from 'rxjs';

export class Car {
    constructor(id, name, model, year, owner, regN, cena) {
        this.id=id;
        this.name = name;
        this.model = model;
        this.year = year;
        this.owner = owner;
        this.regN = regN;
        this.cena=cena;
    }

    crtaj(where) {
        let place = document.getElementById(where);
        let divcic = document.createElement("div");
        divcic.className = "divAuto container-fluid";
        place.appendChild(divcic);
        const { name, model, year, owner, regN, cena } = this;
        divcic.innerHTML = `<h2>${name} ${model}</h2></br>
        godina proizvodnje: ${year} </br>
        vlasnik: ${owner} </br> registarski broj: ${regN} </br> cena: ${cena}e `
        let buttonAzuriraj = document.createElement("button");
        divcic.appendChild(buttonAzuriraj);
        buttonAzuriraj.className = "btn btn-dark";
        buttonAzuriraj.innerHTML = "Azuriraj";
        buttonAzuriraj.id = "azuriraj";
        Rxjs.Observable.fromEvent(buttonAzuriraj, "click")
            .subscribe(ev => {
                document.getElementById("ime").value = name;
                document.getElementById("model").value = model;
                document.getElementById("godina").value = year;
                document.getElementById("vlasnik").value = owner;
                document.getElementById("registarski").value = regN;
                document.getElementById("cena").value=cena;
                document.getElementById("dodaj").innerHTML = "Potvrdi izmene";
            })
    }

    crtajZaIzlog(){
        let divcic = document.createElement("div");
        divcic.className = "divIzlog";
        divSlide.appendChild(divcic);
        const { name, model, year, owner, regN, cena } = this;
        divcic.innerHTML = `<h3>${name} ${model}</h3></br>
        godina proizvodnje: ${year} </br>
        vlasnik: ${owner} </br> registarski broj: ${regN} </br> <b> cena: ${cena}e </b> `
    }
}