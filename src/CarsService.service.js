import * as Rxjs from 'rxjs'
import { Car } from './Car'
import { salon } from './Salon'
import { interval } from 'rxjs/observable/interval';
export class CarsService {


    static get() {
        return fetch("http://localhost:3000/cars/")
            .then(response => response.json())
    }

    static add(salon) {
        let naziv = document.getElementById("ime").value;
        let mod = document.getElementById("model").value;
        let god = document.getElementById("godina").value;
        let reg = document.getElementById("registarski").value;
        let vlasnik = document.getElementById("vlasnik").value;
        let cena=document.getElementById("cena").value;
        let noviAuto = new Car(salon.id,naziv, mod, god, vlasnik, reg,cena);
        salon.Dodaj(noviAuto);
        fetch("http://localhost:3000/cars/", {
                method: 'post',
                headers: {

                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    name: naziv,
                    model: mod,
                    year: god,
                    owner: vlasnik,
                    regN: reg,
                    cena: cena
                })
            })
            .then(response => {
                console.log(response);

            })
    }

    static delete(id) {
        const deleteCar = Rxjs.Observable.fromPromise(
            fetch(`http://localhost:3000/cars/${ id }`, {
                method: 'delete',
                headers: {

                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
            })
            .then(response => {
                console.log(response);
            })
            .catch(err => console.log(err))
        )
    }

    static update(salon,id) {

        let url = "http://localhost:3000/cars/";
        let name = document.getElementById("ime").value;
        let mode= document.getElementById("model").value;
        let godina = document.getElementById("godina").value;
        let vlasnik= document.getElementById("vlasnik").value;
        let registracija= document.getElementById("registarski").value;
        let cena=document.getElementById("cena").value;
        salon.AzurirajElement(id,name,mode,godina,vlasnik,registracija,cena);
        let put = Rxjs.Observable.fromPromise(
            fetch(`${url}${id}`, {
                method: 'put',
                headers: {

                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },

                body: JSON.stringify({
                    name: name,
                    model: mode,
                    year: godina,
                    owner: vlasnik,
                    regN: registracija,
                    cena: cena
                })
            })
            .then(response => {
                console.log(response);
            })
        )
    }

    static clearAll() {
        document.getElementById("ime").value = "";
        document.getElementById("model").value = "";
        document.getElementById("godina").value = "";
        document.getElementById("vlasnik").value = "";
        document.getElementById("registarski").value = "";
        document.getElementById("cena").value="";
    }

    static FindCarForDelete(salon) {
            let a = salon.niz.filter(element => element.regN === document.getElementById("unos").value);
            let id = a[0].id;
            salon.IzbrisiElement(a[0].regN);
            document.getElementById("divPretraga").innerHTML = "";
            document.getElementById("unos").value = "";
            CarsService.delete(id);
            document.getElementById("divZaAutomobile").innerHTML = "";
            salon.niz.forEach(element => element.crtaj("divZaAutomobile"));
        }
    

    static findSugestions(str, niz) {
        let pom = niz.filter(element => element.regN.includes(str));
        return pom;
    }

    static FindIdForUpdate(salon) {
        let identif = 0;
        console.log(document.getElementById("registarski").value);
            salon.niz.forEach(element => {
                if (element.regN === document.getElementById("registarski").value)
                    identif = element.id;

            });
        this.update(salon,identif);
        }

}