import { Car } from "./Car"
import { CarsService } from "./CarsService.service"
import * as Rxjs from 'rxjs';
import { Salon } from "./Salon"
import { timeout } from "rxjs/operators";
import { interval } from "rxjs/observable/interval";


let divSlide= document.createElement("div");
document.body.appendChild(divSlide);
divSlide.className="divSlide";
divSlide.id="divSlide";

let formaDiv = document.createElement("div");
document.body.appendChild(formaDiv);
formaDiv.className = "formaDiv";
formaDiv.id = "formaDiv";


let divZaSve = document.createElement("div");
document.body.appendChild(divZaSve);
divZaSve.className = "divZaSve";
divZaSve.id = "divZaSve";

let button = document.createElement("button");
divZaSve.appendChild(button);
button.className = "btn btn-secondary";
button.innerHTML = "Prikazi automobile";

let divZaAutomobile = document.createElement("div");
divZaSve.appendChild(divZaAutomobile);
divZaAutomobile.className = "container";
divZaAutomobile.id = "divZaAutomobile";

let ime = document.createElement("input");
formaDiv.appendChild(ime);
ime.id = "ime";
ime.placeholder = "Naziv";
let model = document.createElement("input");
formaDiv.appendChild(model);
model.id = "model";
model.placeholder = "Model";
let godina = document.createElement("input");
formaDiv.appendChild(godina);
godina.id = "godina";
godina.placeholder = "Godina";
let vlasnik = document.createElement("input");
formaDiv.appendChild(vlasnik);
vlasnik.id = "vlasnik";
vlasnik.placeholder = "Vlasnik";
let registarski = document.createElement("input");
formaDiv.appendChild(registarski);
registarski.id = "registarski";
registarski.placeholder = "Registarski broj";
let cena = document.createElement("input");
formaDiv.appendChild(cena);
cena.id = "cena";
cena.placeholder = "Cena";

let buttonDodaj = document.createElement("button");
formaDiv.appendChild(buttonDodaj);
buttonDodaj.className = "btn btn-secondary";
buttonDodaj.id = "dodaj";
buttonDodaj.innerHTML = "Dodaj automobil";

let br = document.createElement("br");
formaDiv.appendChild(br);
let label=document.createElement("label");
formaDiv.appendChild(label);
label.innerHTML="Unesite pun registarski broj automobila koji zelite da izbrisete ili azurirate. ";
let b = document.createElement("br");
formaDiv.appendChild(b);
let unos = document.createElement("input");
formaDiv.appendChild(unos);
unos.id = "unos";
unos.placeholder = "Pretraga za brisanje";
let buttonIzbrisi = document.createElement("button");
formaDiv.appendChild(buttonIzbrisi);
buttonIzbrisi.className = "btn btn-warning";
buttonIzbrisi.innerHTML = "Izbrisi automobil";
buttonIzbrisi.id = "brisi";

let divPretraga = document.createElement("div");
formaDiv.appendChild(divPretraga);
divPretraga.id = "divPretraga";



let salon = new Salon();

salon.NapuniNiz();

let obsPrikazi$ = Rxjs.Observable.fromEvent(button, "click");
obsPrikazi$
.subscribe(obj => {
    document.getElementById("divZaAutomobile").innerHTML = "";
    let buttonSakrij = document.createElement("button");
    divZaAutomobile.appendChild(buttonSakrij);
    buttonSakrij.className = "btn btn-warning";
    buttonSakrij.innerHTML = "Sakrij automobile";
    buttonSakrij.id = "Sakrij";
    salon.niz.forEach(element => element.crtaj("divZaAutomobile"));
    Rxjs.Observable.fromEvent(buttonSakrij, "click")
        .subscribe(ev => { document.getElementById("divZaAutomobile").innerHTML = "" })
})



Rxjs.Observable.fromEvent(unos, "input")
    .debounceTime(700)
    .map(ev => ev.target.value)
    .map(l => CarsService.findSugestions(l, salon.niz))
    .subscribe(s => {
        document.getElementById("divPretraga").innerHTML = "";
        if (s.length != salon.niz.length) {
           s.forEach(element => element.crtaj("divPretraga"));
        }
    });


let obsDodaj$ = Rxjs.Observable.fromEvent(buttonDodaj, "click");
obsDodaj$.subscribe(ev => {

    if (document.getElementById("dodaj").innerHTML === "Potvrdi izmene") {
        CarsService.FindIdForUpdate(salon);
        document.getElementById("divPretraga").innerHTML = "";
        document.getElementById("divZaAutomobile").innerHTML = "";
        salon.niz.forEach(element => { element.crtaj("divZaAutomobile") });
        document.getElementById("dodaj").innerHTML = "Dodaj automobil";
        CarsService.clearAll();
    } else {
        let noviAutomobil = CarsService.add(salon);
        CarsService.clearAll();
        document.getElementById("divZaAutomobile").innerHTML = "";
        salon.niz.forEach(element => element.crtaj("divZaAutomobile"));
    }
    // CarsService.clearAll();

})

let obsBrisi$ = Rxjs.Observable.fromEvent(buttonIzbrisi, "click");
obsBrisi$.subscribe(ev => {
    CarsService.FindCarForDelete(salon);

})

let slide$=interval(5000)
    .map(res=>parseInt(Math.random()*salon.niz.length))
    .subscribe(res=> {
        divSlide.innerHTML="";
        salon.niz[res].crtajZaIzlog();
        salon.niz[(res+1)%salon.niz.length].crtajZaIzlog();
        salon.niz[(res+2)%salon.niz.length].crtajZaIzlog();
        salon.niz[(res+3)%salon.niz.length].crtajZaIzlog();   
    })